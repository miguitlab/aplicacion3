<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coches".
 *
 * @property int $id
 * @property string|null $marca
 * @property string|null $fecha
 * @property int|null $precio
 * @property int|null $codCli
 *
 * @property Clientes $codCli0
 */
class Coches extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'precio', 'codCli'], 'integer'],
            [['fecha'], 'safe'],
            [['marca'], 'string', 'max' => 50],
            [['id'], 'unique'],
            [['codCli'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codCli' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Número de coche',
            'marca' => 'Marca',
            'fecha' => 'Fecha',
            'precio' => 'Precio',
            'codCli' => 'Cod Cli',
        ];
    }

    /**
     * Gets query for [[CodCli0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCli0()
    {
        return $this->hasOne(Clientes::className(), ['cod' => 'codCli']);
    }
}
