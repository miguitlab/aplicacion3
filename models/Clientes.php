<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $cod
 * @property string|null $nombre
 *
 * @property Coches[] $coches
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'required'],
            [['cod'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['cod'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Codigo del cliente',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Coches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCoches()
    {
        return $this->hasMany(Coches::className(), ['codCli' => 'cod']);
    }
}
